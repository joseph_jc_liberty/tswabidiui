package app.cooperate.liberty.com.mylotto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PreVerificationRegActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_verification_reg);
        getSupportActionBar().setTitle("Registration -> Step 1");

    }
}
