package app.cooperate.liberty.com.mylotto.presetupsteps;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import app.cooperate.liberty.com.mylotto.R;

public class CredentialsStep extends Fragment implements Step {
    TextInputLayout password,cpassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_credentials_step, container, false);
        //initialize your UI
        password = v.findViewById(R.id.password);
        cpassword = v.findViewById(R.id.cpassword);
        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        if(password.getEditText().getText().toString().trim().isEmpty()) {
            VerificationError error = new VerificationError("Password cannot be empty!");
            return error;
        }else if(password.getEditText().getText().toString().length()<8){
            VerificationError error = new VerificationError("Password cannot be less than 8 characters!");
            return error;
        }else if(!cpassword.getEditText().getText().toString().equals(password.getEditText().getText().toString())){
            VerificationError error = new VerificationError("Passwords do not match!");
            return error;
        }
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Step 3");
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
        Toast.makeText(getActivity(),error.getErrorMessage(),Toast.LENGTH_LONG).show();
    }

}