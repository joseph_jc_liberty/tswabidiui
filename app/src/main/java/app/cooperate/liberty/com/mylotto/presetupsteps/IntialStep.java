package app.cooperate.liberty.com.mylotto.presetupsteps;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import app.cooperate.liberty.com.mylotto.R;

public class IntialStep extends Fragment implements Step {
    TextInputLayout tilBirthdate;
    Calendar myCalendar;
    Boolean canFocus = true;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_intial_step, container, false);

        tilBirthdate = v.findViewById(R.id.til_birthdate);
        tilBirthdate.getEditText().setKeyListener(null);
        myCalendar = Calendar.getInstance();
        tilBirthdate.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Log.d("speed_dbug","bool "+b);
                if (b && canFocus) {
                    Log.d("speed_dbug","FocusTriggered");
                    getBirthdate();
                }
                canFocus=true;
            }
        });

        tilBirthdate.getEditText().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d("speed_dbug", String.valueOf(motionEvent.getAction()));
                if(motionEvent.getAction()==motionEvent.ACTION_UP){
                    getBirthdate();

                    canFocus = false;
                }

                return false;
            }
        });
        //initialize your UI


        return v;
    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onSelected() {
        //update UI when selected
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Step 1");
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public void getBirthdate() {


        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tilBirthdate.getEditText().setText(sdf.format(myCalendar.getTime()));
    }
}