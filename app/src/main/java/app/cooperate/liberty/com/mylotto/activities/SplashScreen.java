package app.cooperate.liberty.com.mylotto.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import app.cooperate.liberty.com.mylotto.R;
import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        View easySplashScreenView = new EasySplashScreen(SplashScreen.this)
//                .withFullScreen()
//                .withTargetActivity(LoginActivity.class)
//                .withSplashTimeOut(2000)
//                .withBackgroundResource(R.color.white)
//                .withHeaderText("Twabidi")
//                .withFooterText("Copyright 2018")
////                .withBeforeLogoText("Twabidi")
//                .withAfterLogoText("Some more details")
//                .create();
//
//
//        setContentView(easySplashScreenView);

        EasySplashScreen config = new EasySplashScreen(SplashScreen.this)
                .withFullScreen()
                .withTargetActivity(LoginActivity.class)
                .withSplashTimeOut(2000)
                .withBackgroundResource(android.R.color.white)
//                .withHeaderText("Twabidi")
                .withFooterText("Copyright 2018")
//                .withBeforeLogoText("My cool company")
                .withLogo(R.drawable.nlalg)
                .withAfterLogoText("Get rich fast");
        //add custom font
//        Typeface pacificoFont = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
//        config.getAfterLogoTextView().setTypeface(pacificoFont);

        //change text color
//        config.getHeaderTextView().setTextColor(getResources().getColor(R.color.yellow));
        config.getFooterTextView().setTextColor(getResources().getColor(R.color.red));
//        config.getHeaderTextView().setTextSize(34);



        //finally create the view
        View easySplashScreenView = config.create();
        setContentView(easySplashScreenView);
    }
}
