package app.cooperate.liberty.com.mylotto.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.cooperate.liberty.com.mylotto.R;

public class EulaActivity extends AppCompatActivity {
    Button btnAccept, btnDecline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eula);
        getSupportActionBar().setTitle("EULA Agreement");
        btnAccept = findViewById(R.id.accept);
        btnDecline = findViewById(R.id.decline);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EulaActivity.this, StepperActivity.class));
                finish();
            }
        });
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
