package app.cooperate.liberty.com.mylotto.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import app.cooperate.liberty.com.mylotto.R;

public class LoginActivity extends AppCompatActivity {
    Button btnJoinUs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnJoinUs = findViewById(R.id.btn_join_us);
        btnJoinUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,EulaActivity.class));
            }
        });
    }
}
